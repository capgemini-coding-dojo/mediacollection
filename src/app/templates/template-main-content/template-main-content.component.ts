import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-main-content',
  templateUrl: './template-main-content.component.html',
  styleUrls: ['./template-main-content.component.scss']
})
export class TemplateMainContentComponent implements OnInit {

  @Input()
  public mainTitle: string;

  @Input()
  public illustration: string;

  constructor() { }

  ngOnInit() {
  }

}
