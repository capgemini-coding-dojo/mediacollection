import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateMainContentComponent } from './template-main-content.component';

describe('TemplateMainContentComponent', () => {
  let component: TemplateMainContentComponent;
  let fixture: ComponentFixture<TemplateMainContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateMainContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateMainContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
