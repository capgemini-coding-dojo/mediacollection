import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateMainContentComponent } from './template-main-content/template-main-content.component';

@NgModule({
  declarations: [ TemplateMainContentComponent ],
  imports: [
    CommonModule
  ],
  exports: [
    TemplateMainContentComponent
  ]
})
export class TemplatesModule { }
