import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  public var1 = 'test';
  public var2 = 'test';
  public var3 = 'test';
  public var4 = 'test';

  constructor() { }

  ngOnInit() {
    console.log('ON INIT PAGE NOT FOUND COMPONENT');
  }

  public var5(): string {
    return 'var5';
  }

}
