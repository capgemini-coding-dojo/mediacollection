import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public title: string;
  public open = true;
  constructor(private router: Router) { }

  ngOnInit() {
    this.title = 'Capgemini DOJO Media';
  }

}
