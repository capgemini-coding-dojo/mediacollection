import { Component, OnInit, AfterContentChecked, OnChanges, AfterViewChecked, DoCheck } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public iNeedToBeDisplayed = 'DISPLAYED';
  public imageSrc = 'assets/codingdojobande.PNG';

  public hideText = false;

  constructor() { }

  ngOnInit() {
    console.log('HOME - ON INIT');
  }

  public handleClick() {
    this.hideText = !this.hideText;
  }

}
