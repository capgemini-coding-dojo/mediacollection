export class ComicBook {

  public id: number;

  public serieId: number;

  public serieName: string;

  public title: string;

  public parutionDate: Date;

  public issueNumber: number;
}
