export class Album {

  masterId: string;

	title: string;

	artist: string;

	type: string;

	year: string;

	style: string[];

	thumb: string[];

	label: string[];

	format: string[];

	barcode: string[];

	genre: string[];

	country: string;

	uri: string;

	cover: string;

	resourceUrl: string;
}
