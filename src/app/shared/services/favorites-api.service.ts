import { HttpClient } from '@angular/common/http';
import { ComicBook } from './../models/comic-book';
import { Album } from './../models/album';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FavoritesApiService {
  private albumCollection: AngularFirestoreCollection<Album>;
  private comicCollection: AngularFirestoreCollection<ComicBook>;

  public albumCollection$: Observable<Album[]>;
  public comicCollection$: Observable<ComicBook[]>;

  constructor(private afs: AngularFirestore, private http: HttpClient) {
    // On récupère les collections depuis la BDD Firebase
    this.albumCollection = afs.collection<Album>('musics');
    this.comicCollection = afs.collection<ComicBook>('comics');

    // Sur chaque collection, on récupère un Observable qui va être
    // déclenché à chaque modification dans les données
    this.albumCollection$ = this.albumCollection.valueChanges().pipe(
      map((data) => {
        console.log('ALBUMS VALUE CHANGE');
        return data;
      })
    );
    this.comicCollection$ = this.comicCollection.valueChanges();
  }


  public addMusicToFavorites(album: Album): Promise<any> {
    return this.albumCollection.doc(album.masterId).set(album).catch((e) => {
      console.log(e);
      throw e;
    });
  }

  public removeMusicFromFavorites(masterId: string): Promise<any> {
    return this.albumCollection.doc(masterId).delete().catch((e) => {
      console.log(e);
      throw e;
    });
  }

  public addComicToFavorites(comic: ComicBook): Promise<any> {
    return this.comicCollection.doc(comic.id.toString()).set(comic).catch((e) => {
      console.log(e);
      throw e;
    });
  }

  public removeComicFromFavorites(id: string): Promise<any> {
    return this.comicCollection.doc(id).delete().catch((e) => {
      console.log(e);
      throw e;
    });
  }
}
