# Commandes utiles

Pour lancer l'application sur le port 4200
```
npm start
```

Pour générer un composant, un service, etc :
```
ng generate component MyComponent
ng generate service MyService
```

Ou utiliser le menu contextuel Visual Studio : clic-droit sur un dossier > Schematics > @schematics/angular > sélectionner le type de composant à générer > entrer le nom du composant à générer.

Cette génération est proposée par Angular CLI. L'intérêt est qu'elle automatise plusieurs choses :
1. Génération des métadonnées du composant (selecteur, lien vers le css)
2. Génération d'un fichier de tests unitaire
3. Déclaration du composant dans le module dans lequel il a été généré


# DOJO

## Exercice 1 - initiation
Suivre les instructions de la page d'accueil.
Liens utiles pour comprendre le data-binding et l'event-binding :
* https://angular.io/guide/displaying-data
* https://angular.io/guide/template-syntax#event-binding


## Exercice 2 - routing
Le routing entre les différentes pages de l'application est géré par le `RouterModule`. Pour l'utiliser, il suffit de déclarer toutes les routes de notre application et d'appeler la méthode `RouterModule.forRoot()` dans le module sur lequel on souhaite utiliser le router.
Le Router va ensuite remplacer l'élément HTML `<router-outlet></router-outlet>` par le composant qui correspond à la route courante.

Actuellement la page d'accueil est affichée via l'élément HTML `<app-home></app-home>` dans le fichier ***ui.component.html***. Le but de l'exercice est de remplacer cet élément par le routeur et d'afficher à la place :
- Le composant `HomeComponent` lorsque la route est **/home** ou **/**
- Le composant `PageNotFoundComponent` lorsque la route n'est pas connue

Compléter ensuite le composant `NavComponent` pour rediriger l'utilisateur sur la route **/home** lors du clic sur *Accueil* **sans utiliser la balise *href***.

Quelques routes sont déjà déclarées dans le fichier `app-routing.module.ts`.

Bon à savoir : il est possible de déclarer plusieurs routeurs, ce qui peut être intéressant si on veut que chaque module puisse gérer ses routes indépendemment afin de garantir plus de réutilisabilité.

Voir plus de documentation : https://angular.io/guide/router


## Exercice 3 - recherche de musiques

### Organisation du module

Dans le dossier **app**, créer un module `MusicModule`. Ce module sera composé de :
1. Un formulaire de recherche des musiques (composant `FormSearchMusicComponent` à créer dans un sous-dossier **components**)
2. Un tableau de résultats (composant `MusicListComponent`)
3. Un conteneur pour organiser les deux premiers composants dans une page (composant `PageMusicComponent` à créer dans un sous-dossier **containers**)
4. Un service `MusicApiService` permettant d'appeler l'API SpringBoot et de stocker les résultats

Le composant `PageMusicComponent` doit utiliser le template `TemplateMainContentComponent` afin de mettre en forme le contenu de manière identique sur toutes les pages du site. Ce composant prend deux propriétés en entrée : un titre et une image (l'image *music.jpg*) est déjà fournie dans le dossier **assets**). On utilisera le property-binding pour passer les entrée au composant.

Attention, pour pouvoir utiliser `TemplateMainContentComponent` au sein du module, il faut importer le module `TemplateModule` (qui exporte déjà le composant).

Configurer ensuite le router pour rediriger le lien du menu vers le composant `PageMusicComponent`.


### Création du formulaire

Ci-dessous le code HTML du formulaire :
```html
<div class="container">
  <form>

    <div class="form-group row">
      <label for="artist">Artiste</label>
      <input name="artist" type="text" class="form-control" id="artist" />
      <div class="alert alert-danger" *ngIf="false">Il faut renseigner au moins l'artiste ou le code-barres</div>
    </div>

    <div class="form-group row">
      <label for="type">Type d'album</label>
      <input name="type" type="text" class="form-control" id="type" />
      <div class="alert alert-danger" *ngIf="false">Champ invalide</div>
    </div>

    <div class="form-group row">
      <label for="barcode">Code barre</label>
      <input name="barcode" type="text" class="form-control" id="barcode" />
      <div class="alert alert-danger" *ngIf="false">Il faut renseigner au moins l'artiste ou le code-barres</div>
    </div>

    <button type="submit" class="btn btn-primary">Chercher</button>
  </form>
</div>
```

A partir de ce formulaire, il faut maintenant (documentation : https://angular.io/guide/forms) :
1. Brancher chaque input du formulaire sur une variable du composant via l'utilisation de la directive `ngModel`. L'utilisation de cette directive nécessite l'import du module `FormsModule`
2. Ajouter une directive `ngSubmit` à la balise `<form>` pour faire réagir le composant lorsque le formulaire est validé
3. Afficher les messages d'erreur lorsque les champs ne sont pas renseignés correctement. Cela nécessite l'ajout d'une référence à la directive `ngForm` comme ceci : `<form #myForm="ngForm">`
    * Le champ **Artiste** est requis si le champ **CodeBarre** n'est pas renseigné
    * Le champ **CodeBarre** est requis si le champ **Artiste** n'est pas renseigné
    * Le champ **Type** n'est pas obligatoire


### Affichage des résultats

Pour le composant `MusicListComponent`, reprendre le code suivant afin de pouvoir afficher des données en attendant d'avoir branché l'API :

```typescript
export class MusicListComponent implements OnInit {

  public listAlbums: Album[];

  constructor() {
    const album1 = { cover: 'assets/music.jpg', title: 'ALBUM1', year: '2000', style: ['STYLE1'], label: ['LABEL1'], barcode: ['123']} as Album;
    const album2 = { cover: 'assets/music.jpg', title: 'ALBUM2', year: '2000', style: ['STYLE1'], label: ['LABEL1'], barcode: ['456']} as Album;
    this.listAlbums = Array.of(album1, album2);
  }
}
```

Ci-dessous le code du template. Il faut modifier ce template pour afficher la liste des Albums initialisée dans le composant. La colonne ***Ajouter aux favoris*** sera utilisée dans l'exercice 5.
```html
<div class="tabresponsive">
  <table class="table table-hover table-striped">
    <thead>
      <tr>
        <th>Ajouter aux favoris</th>
        <th>Cover</th>
        <th>Titre</th>
        <th>Année de parution</th>
        <th>Styles</th>
        <th>Labels</th>
        <th>Codebarres</th>
      </tr>
    </thead>

    <tbody>
      <!-- Insérer la liste des albums ici -->
      <tr>
        <td><button class="btn btn-primary">Ajouter aux favoris</button></td>
        <td><td>
        <td><td>
        <td><td>
        <td><td>
        <td><td>
        <td><td>
      </tbody>
  </table>
</div>
```

## Exercice 4 - Appel à l'API SpringBoot

### Appel HTTP

L'API SpringBoot expose deux endpoints :
> HTTP GET http://localhost:4200/album-api/artist/ARTIST?type=TYPE
>
> HTTP GET http://localhost:4200/album-api/barcode/BARCODE

Ces deux endpoints retournent une liste d'Album. Le DTO Album est déjà présent dans le module `SharedModule`. Le type d'album est un queryParam optionnel. Il va donc falloir exploiter ces deux endpoints via l'utilisation du `HttpClient` (voir documentation ici : https://angular.io/guide/http).

Le port 4200 correspond au port utilisé par le serveur de développement Angular. La redirection vers les API est effectuée grâce à la configuration du proxy (voir fichier **proxy.conf.json**).


Pour pouvoir utiliser le client HTTP d'Angular, il faut :
1. Importer le module `HttpClientModule` sur le module `AppModule`
2. Injecter le service `HttpClient` dans le service `MusicApiService`. L'injection d'un service s'effectue en déclarant le service injectable dans en paramètre du constructeur.

Dans le service `MusicApiService`, déclarer une méthode `public updateResults(artist: string, barcode: string, type: string)` dont le rôle sera de :
1. Appeler la méthode privée `searchAlbumByArtist` ou `searchAlbumByBarcode` en fonction des paramètres fournis en entrée. Chacune de ces deux méthodes devra :
    1. Effectuer l'appel HTTP (`this.http.get`)
    2. Utiliser la méthode `pipe()` pour afficher une popup d'erreur en cas d'échec de la requête ou dans le cas d'absence de résultat (liste vide)
    3. Retourner un `Observable<Album[]>`
2. Souscrire à l'Observable retourné par la méthode appelée via la méthode `subscribe`. Sans cette souscription, l'appel HTTP n'est pas réalisé.
3. A la réception de la réponse de l'appel, stocker les résultats dans un observable de type **Subject**. Cet observable pourra être exposé et utilisé par le composant `MusicListComponent` pour pouvoir afficher le résultat.


> Pourquoi ne pas stocker directement la liste d'albums dans le service au lieu d'utiliser un **Subject** ?
>
> Parce que le composant qui affiche les albums n'est pas le même que celui qui demande la requête HTTP. Le composant `MusicListComponent` ne peut donc pas réaliser le subscribe lui-même. A l'initialisation, en attendant qu'un appel soit réalisé, on récupèrerait une liste vide depuis le service. Or le ChangeDetection d'Angular ne se met en oeuvre que lorsque les propriétés bindées sur le template changent de référence, ce qui n'est pas le cas pour notre liste. Il est donc plus simple de manipuler des observables.


### Afficher le résultat

Côté `MusicListComponent`, il ne reste plus qu'à récupérer l'observable et afficher les données qu'il contient. Pour cela, Angular propose un ***pipe*** `async` qui permet de réaliser la souscription à un observable et de renvoyer le contenu (doc : https://angular.io/guide/observables-in-angular#async-pipe).


On remarquera que la liste de labels et de codebarre peut être conséquente et déformer notre tableau. Pour y remédier, il est possible d'utiliser le pipe `slice` qui va nous permettre d'afficher seulement une sous-partie du tableau.

Afin de permettre à l'utilisateur de visualiser le résultat au complet, on utilisera le composant `ngbTooltip` pour afficher la liste complète lorsque l'utilisateur survole la cellule avec la souris (doc : https://ng-bootstrap.github.io/#/components/tooltip/examples). Pour utiliser ce composant, il est nécéssaire d'importer le module `NgbTooltipModule`.

## Exercice 5 - Ajout d'albums aux favoris

L'API permettant d'ajouter un album aux favoris est déjà fournis (voir ***favorites-api.service.ts***). Ce service utilise des collections Firebase qui est une base de données temps réel via l'utilisation d'observables.

Le but de l'exercice est de créer un bouton ***Ajouter aux favoris*** dans le tableau et de le brancher sur la méthode `addMusicToFavorites()` du service.



## Exercice 6 - Affichage des favoris

Créer un module `FavoritesModule` avec un sous dossier **containers** et un sous-dossier **components**.
* Créer le composant `FavoritesList` dans un sous-dossier **components**
* Créer le composant `PageFavoritesComponent` dans un sous dossier **containers**. De la même manière que pour les musiques, ce composant doit utiliser le template `<app-template-main-content>` et afficher le composant `FavoritesList`

Dans le composant `FavoritesList`, afficher dans le même tableau la liste des musiques et des comics présents dans les favoris. Ci-dessous un début de code HTML. Dans la colonne ***Type***, on mettra ***Comic*** ou ***Album*** en fonction du type d'item affiché (l'ajout de comics se fera dans un prochain exercice) :
```html
<div class="tabresponsive">
  <table class="table table-hover table-striped">
    <thead>
      <tr>
        <th>Supprimer des favoris</th>
        <th>Type</th>
        <th>Titre</th>
        <th>Date de parution</th>
        <th>Plus d'infos</th>
      </tr>
    </thead>

    <tbody>
      <!-- Insérer la liste des favoris ici -->
    </tbody>
  </table>
</div>
```

Créer également un bouton sur la mention *Supprimer des favoris* afin d'appeler la méthode `removeMusicFromFavorites()` du service `FavoritesApiService`.


## Exercice 7 - Utilisation de popups

Au clic sur le bouton ***Plus d'infos*** d'un album, ouvrir une popup affichant toutes les informations disponibles dans un album. Il faut créer pour cela un composant `MusicModalComponent` que l'on ouvrira grâce au service `NgbModal`. Voir la documentation : https://ng-bootstrap.github.io/#/components/modal/examples#component


## Exercice 8 - Recherche des comics

L'API .NET Core expose le endpoint suivant :
> HTTP GET http://localhost:4200/comics-api/Comic?title=TITLE

A l'image de ce qui a déjà été réalisé pour la recherche de musiques, créer un nouveau module `ComicsModule` avec :
* Un composant pour le formulaire de recherche
* Un composant pour lister les résultats
* Un composant ***container***
* Un service pour appeler l'API


Nous avons donc un 3ème tableau à réaliser. Nous allons en profiter pour créer un template de tableau qui va prendre en entrée la liste des headers à afficher et préparer l'affichage du tableau.

Le template doit être créé dans le module `TemplatesModule` et être réutilisé sur toutes les pages du site affichant un tableau.

Ne pas oublier l'ajout et la suppression des favoris, ainsi que l'affichage d'une popup sur la page des favoris.


## Exercice 9 - Pour aller plus loin

Voici une liste non exhaustive d'évolutions pour aller plus loin :
* Créer une animation pour faire disparaître ou apparaître les favoris du composant `FavoritesListComponent` lorsqu'ils sont supprimés/ajoutés. Documentation : https://angular.io/guide/animations
* Implémenter un tri sur les tableaux de résultat
* Afficher un spinner lors des appels aux API en attendant le résultat
* Ajouter un module de login avec des ***guards*** sur les routes pour protéger les pages : https://angular.io/guide/router#milestone-5-route-guards
* Utiliser des ***Reactive Forms*** plutôt que des ***Template-driven forms*** : https://angular.io/guide/reactive-forms
* Créer un ***Pipe*** pour transformer les données : https://angular.io/guide/pipes
* Configurer les routes en mode Lazy Loading : https://angular.io/guide/lazy-loading-ngmodules
* Créer une directive pour rendre réutilisable l'utilisation conjointe du `| slice` et du `ngbTooltip` dans le composant `MusicListComponent`


# Angular CLI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
